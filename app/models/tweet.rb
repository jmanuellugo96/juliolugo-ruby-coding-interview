# == Schema Information
#
# Table name: tweets
#
#  id         :integer          not null, primary key
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#

# class TweetValidation < ActiveModel::Validator
#   def validate(record)
    
#     check = Tweet.find_by(body: record.body, user: record.user_id)
    
#     if !check.nil?
#       if check&.created_at > 24.hours.ago
#         return false
#       end
#     end

#     return true
#   end


# end

class Tweet < ApplicationRecord
  # include ActiveModel::Validations

  scope :unaffiliated_user, lambda { join(:user).where(:company_id => nil)}
  # Ex:- scope :active, lambda {where(:active => true)}
  validate :check_for_duplicate_tweet

  belongs_to :user

  validates :body, length: { maximum: 180}

  private

  def check_for_duplicate_tweet
    existing_tweet = Tweet.where(user_id: user_id, body: body)
                           .where('created_at > ?', 24.hours.ago)
                           .first

    errors.add(:body, "You can't post the same tweet within 24 hours") if existing_tweet
  end

end
