require 'rails_helper'

RSpec.describe Tweet, type: :model do
  let(:user) { create(:user)} 

  it "validates body length is less than 180 chars" do
    tweet = build(:tweet, body: "This is my body and I write it like this")

    expect(tweet).to be_valid 
  end
  
  it "validates tweet if created with same body and same user in previous in 24 hours" do
    tweet = create(:tweet, body: "This is my body", user_id: user.id)
    tweet_two = build(:tweet, body: "This is my body", user_id: user.id)


    expect(tweet_two).to_not be_valid
  end

end
